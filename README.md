# Mathematical models of infectious disease transmission

I presented this talk at the
[Melbourne Maths and Science Meetup](https://www.meetup.com/The-Melbourne-Maths-and-Science-Meetup/)
in [February 2019](https://www.meetup.com/The-Melbourne-Maths-and-Science-Meetup/events/258523294/).

The companion [stochastic outbreak demo](http://mathmodelling.sph.unimelb.edu.au/~rgm/sir-demo/)
was fun. Use the provided ``numbers.pdf`` file to allocate each audience member a unique number
and simulate disease outbreaks where they infect each other!
